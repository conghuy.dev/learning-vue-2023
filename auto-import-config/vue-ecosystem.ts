export default [
  {
    'vue-router': ['createRouter', 'createWebHashHistory'],
  },
  {
    from: 'vue-router',
    imports: ['RouterOptions'],
    type: true,
  },
];
