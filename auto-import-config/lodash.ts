export default [
  {
    'lodash-es': ['get', 'capitalize', 'isString', 'has', 'isNull'],
  },
];
