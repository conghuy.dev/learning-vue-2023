import autoImportVue from './vue';
import autoImportLodash from './lodash';
import autoImportVueEcosystem from './vue-ecosystem';

export default {
  vue: autoImportVue,
  lodash: autoImportLodash,
  vueEcosystem: autoImportVueEcosystem,
};
