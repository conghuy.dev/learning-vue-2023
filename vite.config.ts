import path from 'path';
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import AutoImport from 'unplugin-auto-import/vite';
import autoImportConfig from './auto-import-config';

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: {
      router: path.resolve(__dirname, 'src/router'),
      components: path.resolve(__dirname, 'src/components'),
      containers: path.resolve(__dirname, 'src/containers'),
      config: path.resolve(__dirname, 'src/config'),
      api: path.resolve(__dirname, 'src/api'),
      types: path.resolve(__dirname, 'src/types'),
      styles: path.resolve(__dirname, 'src/styles'),
      pages: path.resolve(__dirname, 'src/pages'),
      i18n: path.resolve(__dirname, 'i18n'),
      utils: path.resolve(__dirname, 'src/utils'),
      assets: path.resolve(__dirname, 'assets'),
      src: path.resolve(__dirname, 'src'),
      modals: path.resolve(__dirname, 'src/modals'),
      enums: path.resolve(__dirname, 'src/enums'),
      store: path.resolve(__dirname, 'src/store'),
      hooks: path.resolve(__dirname, 'src/hooks'),
      libraries: path.resolve(__dirname, 'src/libraries'),
      context: path.resolve(__dirname, 'src/context'),
    },
    extensions: ['.tsx', '.ts', '.jsx', '.js', '.json'],
  },
  plugins: [
    vue(),
    AutoImport({
      eslintrc: {
        enabled: true,
      },
      imports: [
        ...autoImportConfig.vue,
        ...autoImportConfig.lodash,
        ...autoImportConfig.vueEcosystem,
      ] as any,
      dirs: ['src/shared'],
      dts: 'src/types/auto-imports.d.ts',
    }),
  ],
});
