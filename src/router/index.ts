import { Application } from 'components/Application';

const routes: RouterOptions['routes'] = [
  {
    path: '/',
    component: Application,
  },
];

const router = createRouter({ routes, history: createWebHashHistory() });

export default router;
